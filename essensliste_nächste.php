<?php
/*
UserSpice 4
An Open Source PHP User Management System
by the UserSpice Team at http://UserSpice.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
?>
<?php
require_once 'users/init.php';
require_once $abs_us_root.$us_url_root.'users/includes/header.php';
require_once $abs_us_root.$us_url_root.'users/includes/navigation.php';
?>

<?php if (!securePage($_SERVER['PHP_SELF'])){die();} ?>

<head>
<title>Essenwahl Martinschule</title>
<style>
table.db-table 		{ border-right:1px solid #ccc; border-bottom:1px solid #ccc;  }
table.db-table th	{ background:#eee; padding:5px; border-left:1px solid #ccc; border-top:1px solid #ccc;  }
table.db-table td	{ padding:5px; border-left:1px solid #ccc; border-top:1px solid #ccc;  }
</style>
</head>
<div id="page-wrapper">
	<div class="container-fluid">
		<!-- Page Heading -->
		<div class="row">
			<div class="col-sm-12">
				<h1 class="page-header">
				Essensliste für diese Woche
				</h1>
				<!-- Content goes here -->
<?php require 'db_connect.php'; ?>
					<select name="Essen" onchange="location = this.value">
						<option value="0">Bitte auswählen ...</option>
						<option value="essensliste_diese.php">Diese Woche</option>
						<option value="essensliste_nächste.php">Nächste Woche</option>
					</select>
				<br>
				<br>

<?php



	$begin = date('Y-m-d', strtotime('next monday'));
	$end = date('Y-m-d', strtotime('next monday + 4 day'));
	$next_week = ">= $begin and a.datum <= $end";
/*
	echo $next_week; echo "</br>";
	echo $begin; echo "</br>";
	echo $end; echo "</br>";
 */

	$sql = 'SELECT a.datum, g.name AS essengruen, r.name AS essenrot
	FROM essensangebot a
	JOIN essensliste g ON g.id = a.essengruen
	JOIN essensliste r ON r.id = a.essenrot 
	AND a.datum >= "' . $begin . '" AND a.datum <= "' . $end . 
	'" ORDER BY a.datum';
/*
	echo $sql;
 */

	$ergebnis = $mysqli->query($sql);

?>

<?php
echo "<table cellpadding='0' cellspacing='0' class='db-table'\n>";
?>
<tr>
<th>Datum</th><th>Rot</th><th>Grün</th>
</tr>
<?php
while($zeile = $ergebnis->fetch_array()) {
	echo
	"<tr><td>" . htmlspecialchars($zeile["datum"]) . "</td>"
	. "<td>" . htmlspecialchars($zeile["essenrot"]) . "</td>"
	. "<td>" . htmlspecialchars($zeile["essengruen"]) . "</td>"
	. "</tr>\n";
}
echo "</table>";

$ergebnis->close();
$mysqli->close();
?>

</br>
<br>
				<!-- Content Ends Here -->
			</div> <!-- /.col -->
		</div> <!-- /.row -->
	</div> <!-- /.container -->
</div> <!-- /.wrapper -->


<?php require_once $abs_us_root.$us_url_root.'users/includes/page_footer.php'; // the final html footer copyright row + the external js calls ?>

<!-- Place any per-page javascript here -->

<?php require_once $abs_us_root.$us_url_root.'users/includes/html_footer.php'; // currently just the closing /body and /html ?>
