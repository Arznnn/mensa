<?php
require_once 'users/init.php';
require_once $abs_us_root.$us_url_root.'users/includes/header.php';
require_once $abs_us_root.$us_url_root.'users/includes/navigation.php';
?>

<?php if (!securePage($_SERVER['PHP_SELF'])){die();} ?>

<head>
	<style>
	table.db-table 		{ border-right:1px solid #ccc; border-bottom:1px solid #ccc;  }
	table.db-table th	{ background:#eee; padding:5px; border-left:1px solid #ccc; border-top:1px solid #ccc;  }
	table.db-table td	{ padding:5px; border-left:1px solid #ccc; border-top:1px solid #ccc;  }
	</style>
</head>

<div id="page-wrapper">
	<div class="container-fluid">
		<!-- Page Heading -->
		<div class="row">
			<div class="col-sm-12">
				<h1 class="page-header">
				Hallo <?php echo $user->data()->fname;?>!<br> Hier wählst du dein Essen
				</h1>
				<!-- Content goes here -->
				<?php
				require 'db_connect.php';
				?>
				  <select name="Essen" onchange="location = this.value">
				     <option value="0">Bitte auswählen ...</option>
				     <option value="essensauswahl_diese.php">Diese Woche</option>
				     <option value="essensauswahl.php">Nächste Woche</option>
				    </select>
				<br>
				<br>

				<form action="" method="post">
				<?php
				$userid = $user->data()->id;
				$farbe = 'rot';
				$datum = '2019-03-02';

				$begin = date('Y-m-d', strtotime('next monday'));
				$end = date('Y-m-d', strtotime('next monday + 4 day'));

				$ergebnis = $mysqli->query("SELECT a.datum, g.name AS essengruen, r.name AS essenrot
					FROM essensangebot a
					JOIN essensliste g ON g.id = a.essengruen
					JOIN essensliste r ON r.id = a.essenrot and a.datum >= '$begin' and a.datum <= '$end'
					ORDER BY a.datum");
				?>
				<?php
				echo "<table cellpadding='0' cellspacing='0' class='db-table'\n>";
				?>
				<tr>
				<th></th><th>Datum</th><th colspan="2">Rot</th><th colspan="2">Grün</th><th>Nix</th>
				</tr>
				<?php
				$i = 1;
				while($zeile = $ergebnis->fetch_array()) {
					echo
					"<tr>
					  <td>" . htmlspecialchars($i) . "</td>"
					. "<td>" . $zeile['datum'] . "</td>" 
					. "<td> <input type='radio' name='$i' value='rot'> </td>"
					. "<td>" . htmlspecialchars($zeile["essenrot"]) . "</td>"
					. "<td> <input type='radio' name='$i' value='gruen'> </td>"
					. "<td>" . htmlspecialchars($zeile["essengruen"]) . "</td>"
					. "<td> <input type='radio' name='$i' checked='checked'> </td></tr>\n";
				if (isset($_POST['aktion']) and $_POST['aktion']=='speichern') {
					$farbe = $_POST[$i];
					$datum = $zeile['datum'];
					$einfuegen = $mysqli->prepare(
				              "INSERT INTO essensauswahl (user, farbe, date)
				                VALUES (?, ?, ?)"
				               );
					$einfuegen->bind_param('iss' , $userid, $farbe ,$datum);
					$einfuegen->execute();
					$einfuegen->close();
				}
				$i++;
				}
				echo "</table>";
				?>
				 <input type="hidden" name="aktion" value="speichern"> 
			    	 <input type="submit" value="Speichern">
				</form>
				<?php
				if($ergebnis) echo "Datensatz wurde gespeichert.";
				$ergebnis->close();
				$mysqli->close();
				?>
<br>
<br>

				<!-- Content Ends Here -->
			</div> <!-- /.col -->
		</div> <!-- /.row -->
	</div> <!-- /.container -->
</div> <!-- /.wrapper -->


<?php require_once $abs_us_root.$us_url_root.'users/includes/page_footer.php'; // the final html footer copyright row + the external js calls ?>

<!-- Place any per-page javascript here -->

<?php require_once $abs_us_root.$us_url_root.'users/includes/html_footer.php'; // currently just the closing /body and /html ?>
