<?php

if(file_exists("install/index.php")){
	//perform redirect if installer files exist
	//this if{} block may be deleted once installed
	header("Location: install/index.php");
}
require_once 'users/init.php';
require_once $abs_us_root.$us_url_root.'users/includes/header.php';
require_once $abs_us_root.$us_url_root.'users/includes/navigation.php';

?>
<div id="page-wrapper">
	<div class="container">
		<div class="jumbotron">
			<h1 align="center">Willkommen zur Martinschul-Mensa</h1>
			<p align="center" class="text-muted">An Open Source PHP Meal Management Framework. </p>
			<p align="center">
<?php
				if($user->isLoggedIn()){?>
					<a class="btn btn-primary" href="users/account.php" role="button">Dein Account &raquo;</a>
				<?php }else{?>
					<a class="btn btn-warning" href="users/login.php" role="button">Anmelden&raquo;</a>
					<a class="btn btn-info" href="users/join.php" role="button">Registrieren&raquo;</a>
				<?php }	?>
			</p>
			<br>
			<p align="center">Du bist auf den Seiten der Mensa Martinschule!<br>
			Beginne gleich deine Essenswünsche einzugeben :-)</p>
		</div>
	</div>
</div>

<!-- Place any per-page javascript here -->


<?php require_once $abs_us_root . $us_url_root . 'usersc/templates/' . $settings->template . '/footer.php'; //custom template footer ?>
