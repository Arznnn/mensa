<?php
$servername = "localhost";
$username = "meinprojekt";
$password = "meinpasswort";
$db = "DB";

$mysqli = new mysqli($servername, $username, $password, $db);
if ($mysqli->connect_error) {
	echo "Fehler bei der Verbindung:" . mysqli_connect_error();
	exit();
}

if (!$mysqli->set_charset("utf8")) {
  echo "Fehler beim Laden von UTF8 ". $mysqli->error;
}
?>
