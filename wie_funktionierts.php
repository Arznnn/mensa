<?php
/*
UserSpice 4
An Open Source PHP User Management System
by the UserSpice Team at http://UserSpice.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
?>
<?php
require_once 'users/init.php';
require_once $abs_us_root.$us_url_root.'users/includes/header.php';
require_once $abs_us_root.$us_url_root.'users/includes/navigation.php';
?>

<?php if (!securePage($_SERVER['PHP_SELF'])){die();} ?>

<div id="page-wrapper">
	<div class="container-fluid">
		<!-- Page Heading -->
		<div class="row">
			<div class="col-sm-12">
				<h1 class="page-header">
					Wie funktionierts?
				</h1>
				<!-- Content goes here -->
				<p>Wenn du sehen willst, welches Essen es in den n&aumlchsten zwei Wochen gibt, klicke auf <a href="http://localhost/php/schule/user/essensliste.php">"Essensliste"</a>.
				<br>
				Wenn du Essen w&aumlhlen willst klicke auf <a href="essensauswahl.php">"Essensauswahl"</a> und melde dich mit deinem Namen und dem Passwort, welches du von deinem Lehrer bekommen hast an.
				Dann kannst du alleine oder mit deinen Eltern dein Essen für die n&aumlchsten zwei Wochen w&aumlhlen.
				<br>
				Wenn du im nachhinein noch mal sehen willst, was du gew&aumlhlt hast, klicke auf <a href="essensausgabe.php">"Essensausgabe"</a>.
				Dort schreibst du deinen Namen in das K&aumlstchen.
				Und kannst dann sehen was du f&uumlr wann gew&aumlhlt hast. </p>
				</section>
				<aside id="sidebar">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud </p>
				</aside>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

				<!-- Content Ends Here -->
			</div> <!-- /.col -->
		</div> <!-- /.row -->
	</div> <!-- /.container -->
</div> <!-- /.wrapper -->


<?php require_once $abs_us_root.$us_url_root.'users/includes/page_footer.php'; // the final html footer copyright row + the external js calls ?>

<!-- Place any per-page javascript here -->

<?php require_once $abs_us_root.$us_url_root.'users/includes/html_footer.php'; // currently just the closing /body and /html ?>
