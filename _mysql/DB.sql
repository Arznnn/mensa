-- MySQL dump 10.17  Distrib 10.3.13-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: DB
-- ------------------------------------------------------
-- Server version	10.3.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `audit`
--

DROP TABLE IF EXISTS `audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `page` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `ip` varchar(255) NOT NULL,
  `viewed` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audit`
--

LOCK TABLES `audit` WRITE;
/*!40000 ALTER TABLE `audit` DISABLE KEYS */;
INSERT INTO `audit` VALUES (1,0,'4','2019-03-03 16:48:26','::1',0),(2,11,'92','2019-03-03 18:16:05','::1',0),(3,11,'92','2019-03-03 18:16:16','::1',0),(4,11,'92','2019-03-03 18:16:21','::1',0),(5,11,'4','2019-03-03 18:16:30','::1',0),(6,11,'4','2019-03-03 18:16:32','::1',0),(7,11,'4','2019-03-03 18:16:34','::1',0),(8,11,'92','2019-03-03 18:18:14','::1',0),(9,2,'4','2019-03-03 18:30:08','::1',0),(10,2,'4','2019-03-03 18:32:55','::1',0),(11,2,'92','2019-03-03 18:33:37','::1',0),(12,2,'92','2019-03-03 18:33:39','::1',0),(13,2,'92','2019-03-03 18:33:43','::1',0),(14,2,'92','2019-03-03 18:33:53','::1',0),(15,0,'4','2019-03-03 18:35:44','::1',0),(16,2,'4','2019-03-03 18:35:58','::1',0),(17,12,'94','2019-03-03 20:31:04','::1',0),(18,12,'94','2019-03-03 20:31:24','::1',0),(19,12,'94','2019-03-03 20:31:38','::1',0),(20,12,'94','2019-03-03 20:31:53','::1',0),(21,12,'94','2019-03-03 20:34:48','::1',0),(22,12,'94','2019-03-03 20:34:50','::1',0),(23,12,'94','2019-03-03 20:34:55','::1',0),(24,12,'94','2019-03-03 20:35:15','::1',0),(25,12,'94','2019-03-03 20:35:31','::1',0),(26,12,'94','2019-03-03 20:39:16','::1',0),(27,12,'94','2019-03-03 20:39:23','::1',0),(28,12,'94','2019-03-03 20:39:38','::1',0),(29,12,'94','2019-03-03 20:39:41','::1',0),(30,12,'94','2019-03-03 20:39:44','::1',0);
/*!40000 ALTER TABLE `audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `crons`
--

DROP TABLE IF EXISTS `crons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` int(1) NOT NULL DEFAULT 1,
  `sort` int(3) NOT NULL,
  `name` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL,
  `createdby` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `crons`
--

LOCK TABLES `crons` WRITE;
/*!40000 ALTER TABLE `crons` DISABLE KEYS */;
INSERT INTO `crons` VALUES (1,0,100,'Auto-Backup','backup.php',1,'2017-09-16 07:49:22','2017-11-11 20:15:36');
/*!40000 ALTER TABLE `crons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `crons_logs`
--

DROP TABLE IF EXISTS `crons_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crons_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cron_id` int(11) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `crons_logs`
--

LOCK TABLES `crons_logs` WRITE;
/*!40000 ALTER TABLE `crons_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `crons_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email`
--

DROP TABLE IF EXISTS `email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `website_name` varchar(100) NOT NULL,
  `smtp_server` varchar(100) NOT NULL,
  `smtp_port` int(10) NOT NULL,
  `email_login` varchar(150) NOT NULL,
  `email_pass` varchar(100) NOT NULL,
  `from_name` varchar(100) NOT NULL,
  `from_email` varchar(150) NOT NULL,
  `transport` varchar(255) NOT NULL,
  `verify_url` varchar(255) NOT NULL,
  `email_act` int(1) NOT NULL,
  `debug_level` int(1) NOT NULL DEFAULT 0,
  `isSMTP` int(1) NOT NULL DEFAULT 0,
  `isHTML` varchar(5) NOT NULL DEFAULT 'true',
  `useSMTPauth` varchar(6) NOT NULL DEFAULT 'true',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email`
--

LOCK TABLES `email` WRITE;
/*!40000 ALTER TABLE `email` DISABLE KEYS */;
INSERT INTO `email` VALUES (1,'User Spice','smtp.gmail.com',587,'yourEmail@gmail.com','1234','User Spice','yourEmail@gmail.com','tls','http://localhost/43',0,0,0,'true','true');
/*!40000 ALTER TABLE `email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `essensangebot`
--

DROP TABLE IF EXISTS `essensangebot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `essensangebot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `essenrot` int(11) DEFAULT NULL,
  `essengruen` int(11) DEFAULT NULL,
  `datum` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `essensangebot`
--

LOCK TABLES `essensangebot` WRITE;
/*!40000 ALTER TABLE `essensangebot` DISABLE KEYS */;
INSERT INTO `essensangebot` VALUES (1,6,7,'2019-03-04'),(2,1,5,'2019-03-05'),(5,9,2,'2019-03-08'),(6,9,7,'2019-03-03'),(7,4,3,'2019-03-11'),(8,9,7,'2019-03-12'),(9,10,2,'2019-03-13'),(10,9,5,'2019-03-14'),(11,1,7,'2019-03-15'),(14,4,2,'2019-03-06'),(15,1,5,'2019-03-07'),(17,4,7,'2019-03-27'),(18,14,13,'2019-03-25'),(19,12,13,'2019-03-26'),(20,10,11,'2019-03-28'),(21,9,5,'2019-03-29'),(22,15,7,'2019-03-18'),(23,16,17,'2019-04-01'),(24,10,2,'2019-04-01'),(25,12,7,'2019-04-02'),(26,4,13,'2019-04-03'),(27,12,11,'2019-04-04'),(28,10,5,'2019-04-05');
/*!40000 ALTER TABLE `essensangebot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `essensauswahl`
--

DROP TABLE IF EXISTS `essensauswahl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `essensauswahl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) DEFAULT NULL,
  `farbe` varchar(7) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `essensauswahl`
--

LOCK TABLES `essensauswahl` WRITE;
/*!40000 ALTER TABLE `essensauswahl` DISABLE KEYS */;
INSERT INTO `essensauswahl` VALUES (1,13,'rot','2019-03-04'),(2,13,'rot','2019-03-05'),(3,13,'on','2019-03-06'),(4,13,'rot','2019-03-07'),(5,13,'gruen','2019-03-08'),(6,1,'gruen','2019-03-11'),(7,1,'rot','2019-03-12'),(8,1,'gruen','2019-03-13'),(9,1,'gruen','2019-03-14'),(10,1,'gruen','2019-03-15'),(11,14,'rot','2019-03-11'),(12,14,'gruen','2019-03-12'),(13,14,'gruen','2019-03-13'),(14,14,'gruen','2019-03-14'),(15,14,'rot','2019-03-15'),(16,14,'rot','2019-03-04'),(17,14,'gruen','2019-03-05'),(18,14,'on','2019-03-05'),(19,14,'rot','2019-03-06'),(20,14,'on','2019-03-06'),(21,14,'gruen','2019-03-07'),(22,14,'on','2019-03-07'),(23,14,'rot','2019-03-08'),(24,14,'on','2019-03-08'),(25,1,'gruen','2019-03-27'),(26,15,'rot','2019-03-25'),(27,15,'gruen','2019-03-26'),(28,15,'gruen','2019-03-27'),(29,15,'gruen','2019-03-28'),(30,15,'gruen','2019-03-29'),(31,2,'gruen','2019-03-25'),(32,2,'rot','2019-03-26'),(33,2,'gruen','2019-03-27'),(34,2,'gruen','2019-03-28'),(35,2,'gruen','2019-03-29'),(36,2,'on','2019-04-01'),(37,2,'gruen','2019-04-02'),(38,2,'rot','2019-04-03'),(39,2,'gruen','2019-04-04'),(40,2,'rot','2019-04-05');
/*!40000 ALTER TABLE `essensauswahl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `essensliste`
--

DROP TABLE IF EXISTS `essensliste`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `essensliste` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `farbe` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `essensliste`
--

LOCK TABLES `essensliste` WRITE;
/*!40000 ALTER TABLE `essensliste` DISABLE KEYS */;
INSERT INTO `essensliste` VALUES (1,'Austern','rot'),(2,'Trüffel Gnocci','grün'),(3,'Käsesuppe','grün'),(4,'himmel und äd','rot'),(5,'Haferkekse','grün'),(7,'Bratapfel','grün'),(8,'Krapfen','grün'),(9,'Paella','rot'),(10,'Schweinshackse','rot'),(11,'Pommes','grün'),(13,'Germknödel','Grün'),(14,'Auflauf','rot'),(15,'Klöse','rot');
/*!40000 ALTER TABLE `essensliste` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups_menus`
--

DROP TABLE IF EXISTS `groups_menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups_menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(15) NOT NULL,
  `menu_id` int(15) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`),
  KEY `menu_id` (`menu_id`)
) ENGINE=MyISAM AUTO_INCREMENT=133 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups_menus`
--

LOCK TABLES `groups_menus` WRITE;
/*!40000 ALTER TABLE `groups_menus` DISABLE KEYS */;
INSERT INTO `groups_menus` VALUES (30,2,9),(29,0,8),(28,0,7),(27,0,21),(5,0,3),(43,0,1),(7,0,2),(8,0,51),(9,0,52),(123,2,37),(125,2,38),(117,0,39),(132,3,40),(109,1,41),(120,3,39),(126,0,43),(17,2,44),(18,2,45),(19,0,46),(20,0,47),(21,0,49),(26,0,20),(25,0,18),(31,2,10),(32,2,11),(33,2,12),(34,2,13),(35,2,14),(36,2,15),(37,0,16),(101,1,23),(96,0,19),(95,0,6),(100,0,23),(44,1,1),(45,2,1),(46,3,1),(97,0,24),(128,3,25),(127,2,25),(121,2,26),(60,0,27),(66,2,28),(68,2,29),(73,2,30),(124,2,31),(74,0,32),(99,0,33),(76,0,34),(122,2,36),(84,3,35),(83,2,35),(82,1,35),(104,0,4),(102,2,23),(103,3,23),(131,2,40),(110,2,41),(119,2,39),(118,1,39),(116,0,42);
/*!40000 ALTER TABLE `groups_menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `keys`
--

DROP TABLE IF EXISTS `keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stripe_ts` varchar(255) NOT NULL,
  `stripe_tp` varchar(255) NOT NULL,
  `stripe_ls` varchar(255) NOT NULL,
  `stripe_lp` varchar(255) NOT NULL,
  `recap_pub` varchar(100) NOT NULL,
  `recap_pri` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `keys`
--

LOCK TABLES `keys` WRITE;
/*!40000 ALTER TABLE `keys` DISABLE KEYS */;
/*!40000 ALTER TABLE `keys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(3) NOT NULL,
  `logdate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `logtype` varchar(25) NOT NULL,
  `lognote` text NOT NULL,
  `ip` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=212 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logs`
--

LOCK TABLES `logs` WRITE;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
INSERT INTO `logs` VALUES (1,1,'2019-03-02 20:33:36','System Updates','Update 2ZB9mg1l0JXe successfully deployed.','::1'),(2,1,'2019-03-02 20:33:36','System Updates','Update 4vsCCmI9CuNZ successfully deployed.','::1'),(3,1,'2019-03-02 20:33:36','System Updates','Update B9t6He7qmFXa: No change required','::1'),(4,1,'2019-03-02 20:33:36','System Updates','Update B9t6He7qmFXa: Successfully deployed','::1'),(5,2,'2019-03-02 20:33:48','User','User logged in.',NULL),(6,1,'2019-03-02 20:34:20','User','User logged in.',NULL),(7,1,'2019-03-02 20:35:35','Permissions Manager','Added Permission Level named K&uuml;che.','::1'),(8,2,'2019-03-03 13:16:12','User','User logged in.',NULL),(9,2,'2019-03-03 13:16:42','Messaging','Archived Thread 2.','::1'),(10,2,'2019-03-03 13:16:42','Messaging','Archived Thread 1.','::1'),(11,1,'2019-03-03 13:17:00','User','User logged in.',NULL),(12,1,'2019-03-03 13:17:41','Menu Manager','Added new item','::1'),(13,1,'2019-03-03 13:18:17','Menu Manager','Updated 23','::1'),(14,1,'2019-03-03 13:19:14','Menu Manager','Updated 1','::1'),(15,1,'2019-03-03 13:20:32','Menu Manager','Updated 1','::1'),(16,1,'2019-03-03 13:20:45','Menu Manager','Deleted menu 2','::1'),(17,1,'2019-03-03 13:20:52','Menu Manager','Deleted menu 17','::1'),(18,1,'2019-03-03 13:21:06','Menu Manager','Deleted menu 18','::1'),(19,1,'2019-03-03 13:27:34','Menu Manager','Updated 23','::1'),(20,1,'2019-03-03 13:27:47','Menu Manager','Added new dropdown','::1'),(21,1,'2019-03-03 13:27:54','Menu Manager','Deleted menu 24','::1'),(22,1,'2019-03-03 13:38:56','Menu Manager','Updated 23','::1'),(23,1,'2019-03-03 13:45:50','User','User logged in.',NULL),(24,1,'2019-03-03 13:46:17','Pages Manager','Changed private from private to public for Page #97 and stripped re_auth.','::1'),(25,1,'2019-03-03 13:46:17','Pages Manager','Retitled \'wie_funktionierts.php\' to \'Help\'.','::1'),(26,1,'2019-03-03 13:46:39','Pages Manager','Changed private from private to public for Page #96 and stripped re_auth.','::1'),(27,1,'2019-03-03 13:46:39','Pages Manager','Retitled \'essenstage.php\' to \'Essenstage\'.','::1'),(28,1,'2019-03-03 13:47:12','Menu Manager','Added new item','::1'),(29,1,'2019-03-03 13:48:00','Menu Manager','Updated 24','::1'),(30,1,'2019-03-03 13:55:20','Menu Manager','Added new dropdown','::1'),(31,1,'2019-03-03 13:55:56','Menu Manager','Updated 25','::1'),(32,1,'2019-03-03 14:00:42','Menu Manager','Added new item','::1'),(33,1,'2019-03-03 14:01:07','Menu Manager','Updated 26','::1'),(34,1,'2019-03-03 14:01:37','Menu Manager','Updated 26','::1'),(35,1,'2019-03-03 14:02:17','Menu Manager','Added new item','::1'),(36,1,'2019-03-03 14:04:47','Menu Manager','Updated 27','::1'),(37,1,'2019-03-03 14:05:17','Menu Manager','Updated 24','::1'),(38,1,'2019-03-03 14:05:41','Menu Manager','Updated 25','::1'),(39,1,'2019-03-03 14:06:19','Menu Manager','Updated 24','::1'),(40,1,'2019-03-03 14:24:11','Menu Manager','Added new item','::1'),(41,1,'2019-03-03 14:29:37','Menu Manager','Updated 28','::1'),(42,1,'2019-03-03 14:30:15','Menu Manager','Updated 28','::1'),(43,1,'2019-03-03 14:32:01','Menu Manager','Added new dropdown','::1'),(44,1,'2019-03-03 14:33:03','Menu Manager','Updated 29','::1'),(45,1,'2019-03-03 14:34:28','Menu Manager','Updated 29','::1'),(46,1,'2019-03-03 14:39:40','Menu Manager','Added new item','::1'),(47,1,'2019-03-03 14:40:07','Menu Manager','Updated 30','::1'),(48,1,'2019-03-03 14:40:24','Menu Manager','Added new item','::1'),(49,1,'2019-03-03 14:40:43','Menu Manager','Updated 31','::1'),(50,1,'2019-03-03 14:41:26','Menu Manager','Updated 31','::1'),(51,1,'2019-03-03 14:41:53','Menu Manager','Updated 30','::1'),(52,1,'2019-03-03 14:42:10','Menu Manager','Added new item','::1'),(53,1,'2019-03-03 14:42:44','Menu Manager','Updated 32','::1'),(54,1,'2019-03-03 14:45:29','Menu Manager','Added new item','::1'),(55,1,'2019-03-03 14:47:01','Menu Manager','Updated 33','::1'),(56,1,'2019-03-03 14:47:51','Menu Manager','Added new item','::1'),(57,1,'2019-03-03 14:48:24','Menu Manager','Updated 34','::1'),(58,1,'2019-03-03 14:48:43','Menu Manager','Added new item','::1'),(59,1,'2019-03-03 14:49:17','Menu Manager','Updated 35','::1'),(60,1,'2019-03-03 14:49:43','Menu Manager','Updated 35','::1'),(61,1,'2019-03-03 14:50:01','Menu Manager','Updated 35','::1'),(62,1,'2019-03-03 14:50:54','Menu Manager','Added new item','::1'),(63,1,'2019-03-03 14:52:02','Menu Manager','Updated 36','::1'),(64,1,'2019-03-03 14:52:07','Menu Manager','Added new item','::1'),(65,1,'2019-03-03 14:53:04','Menu Manager','Updated 37','::1'),(66,1,'2019-03-03 14:53:23','Menu Manager','Added new item','::1'),(67,1,'2019-03-03 14:53:44','Menu Manager','Updated 38','::1'),(68,1,'2019-03-03 14:53:45','Menu Manager','Added new item','::1'),(69,1,'2019-03-03 14:55:25','Menu Manager','Updated 39','::1'),(70,1,'2019-03-03 14:57:05','User','User logged in.',NULL),(71,1,'2019-03-03 14:57:55','Menu Manager','Updated 23','::1'),(72,1,'2019-03-03 14:59:25','Menu Manager','Updated 4','::1'),(73,1,'2019-03-03 15:01:29','Menu Manager','Updated 23','::1'),(74,1,'2019-03-03 15:02:15','Menu Manager','Updated 6','::1'),(75,1,'2019-03-03 15:02:29','Menu Manager','Updated 19','::1'),(76,1,'2019-03-03 15:02:41','Menu Manager','Updated 24','::1'),(77,1,'2019-03-03 15:03:12','Menu Manager','Deleted menu 35','::1'),(78,1,'2019-03-03 15:04:32','Menu Manager','Updated 33','::1'),(79,1,'2019-03-03 15:05:17','Menu Manager','Updated 33','::1'),(80,1,'2019-03-03 15:06:29','Menu Manager','Updated 23','::1'),(81,1,'2019-03-03 15:07:01','User','User logged in.',NULL),(82,1,'2019-03-03 15:11:05','User','User logged in.',NULL),(83,1,'2019-03-03 15:11:26','Menu Manager','Updated 4','::1'),(84,1,'2019-03-03 15:12:09','Menu Manager','Added new item','::1'),(85,1,'2019-03-03 15:13:26','Menu Manager','Updated 40','::1'),(86,1,'2019-03-03 15:13:52','Menu Manager','Updated 40','::1'),(87,1,'2019-03-03 15:14:31','Menu Manager','Added new item','::1'),(88,1,'2019-03-03 15:15:31','Menu Manager','Updated 41','::1'),(89,1,'2019-03-03 15:16:37','Menu Manager','Added new item','::1'),(90,1,'2019-03-03 15:17:31','Menu Manager','Updated 42','::1'),(91,1,'2019-03-03 15:23:00','Menu Manager','Updated 42','::1'),(92,1,'2019-03-03 15:23:29','Menu Manager','Updated 42','::1'),(93,1,'2019-03-03 15:25:28','User','User logged in.',NULL),(94,1,'2019-03-03 15:58:55','User','User logged in.',NULL),(95,1,'2019-03-03 16:48:43','User','User logged in.',NULL),(96,1,'2019-03-03 17:14:59','User','User logged in.',NULL),(97,1,'2019-03-03 17:39:45','User','User logged in.',NULL),(98,1,'2019-03-03 17:52:04','User','User logged in.',NULL),(99,1,'2019-03-03 18:11:55','User','User logged in.',NULL),(100,11,'2019-03-03 18:15:28','User','Registration completed.','::1'),(101,11,'2019-03-03 18:15:51','User','User logged in.',NULL),(102,1,'2019-03-03 18:19:33','User','User logged in.',NULL),(103,1,'2019-03-03 18:28:28','Menu Manager','Updated 39','::1'),(104,2,'2019-03-03 18:28:54','User','User logged in.',NULL),(105,1,'2019-03-03 18:30:19','User','User logged in.',NULL),(106,1,'2019-03-03 18:31:25','Menu Manager','Updated 26','::1'),(107,1,'2019-03-03 18:31:45','Menu Manager','Updated 36','::1'),(108,1,'2019-03-03 18:32:03','Menu Manager','Updated 37','::1'),(109,1,'2019-03-03 18:32:16','Menu Manager','Updated 31','::1'),(110,2,'2019-03-03 18:32:43','User','User logged in.',NULL),(111,1,'2019-03-03 18:33:03','User','User logged in.',NULL),(112,1,'2019-03-03 18:33:17','Menu Manager','Updated 38','::1'),(113,2,'2019-03-03 18:33:29','User','User logged in.',NULL),(114,1,'2019-03-03 18:34:03','User','User logged in.',NULL),(115,1,'2019-03-03 18:34:36','Pages Manager','Changed private from private to public for Page #92 and stripped re_auth.','::1'),(116,1,'2019-03-03 18:34:43','Pages Manager','Added 1 permission(s) to essensauswahl.php.','::1'),(117,1,'2019-03-03 18:34:57','Pages Manager','Changed private from private to public for Page #95 and stripped re_auth.','::1'),(118,1,'2019-03-03 18:34:58','Pages Manager','Added 3 permission(s) to essensliste.php.','::1'),(119,1,'2019-03-03 18:35:13','User','User logged in.',NULL),(120,2,'2019-03-03 18:35:57','User','User logged in.',NULL),(121,2,'2019-03-03 20:04:49','User','User logged in.',NULL),(122,1,'2019-03-03 20:05:17','User','User logged in.',NULL),(123,1,'2019-03-03 20:05:39','Menu Manager','Added new item','::1'),(124,1,'2019-03-03 20:07:47','Menu Manager','Updated 43','::1'),(125,1,'2019-03-03 20:08:03','Pages Manager','Added 3 permission(s) to essenswahl.php.','::1'),(126,1,'2019-03-03 20:25:19','User','User logged in.',NULL),(127,12,'2019-03-03 20:28:05','User','Registration completed.','::1'),(128,1,'2019-03-03 20:28:19','User','User logged in.',NULL),(129,12,'2019-03-03 20:29:38','User','User logged in.',NULL),(130,12,'2019-03-03 20:30:43','User','User logged in.',NULL),(131,1,'2019-03-03 20:32:18','User','User logged in.',NULL),(132,1,'2019-03-03 20:33:22','Menu Manager','Updated 25','::1'),(133,12,'2019-03-03 20:34:45','User','User logged in.',NULL),(134,1,'2019-03-03 20:35:52','User','User logged in.',NULL),(135,1,'2019-03-03 20:36:43','Menu Manager','Updated 40','::1'),(136,1,'2019-03-03 20:37:37','Menu Manager','Updated 40','::1'),(137,12,'2019-03-03 20:38:35','User','User logged in.',NULL),(138,1,'2019-03-03 20:40:02','User','User logged in.',NULL),(139,1,'2019-03-03 20:40:45','Permission Manager','Added 3 pages(s) to Permission #3.','::1'),(140,1,'2019-03-03 20:41:51','Pages Manager','Changed private from private to public for Page #94 and stripped re_auth.','::1'),(141,1,'2019-03-03 20:41:51','Pages Manager','Retitled \'essens_eingabe.php\' to \'K&uuml;che\'.','::1'),(142,12,'2019-03-03 20:42:36','User','User logged in.',NULL),(143,13,'2019-03-03 21:02:35','User','Registration completed.','::1'),(144,13,'2019-03-03 21:03:24','User','User logged in.',NULL),(145,2,'2019-03-03 21:10:29','User','User logged in.',NULL),(146,1,'2019-03-03 21:12:31','User','User logged in.',NULL),(147,2,'2019-03-03 21:13:53','User','User logged in.',NULL),(148,1,'2019-03-03 21:15:29','User','User logged in.',NULL),(149,1,'2019-03-05 09:35:33','User','User logged in.',NULL),(150,2,'2019-03-05 09:54:00','User','User logged in.',NULL),(151,1,'2019-03-05 09:54:17','User','User logged in.',NULL),(152,1,'2019-03-05 09:59:51','User','User logged in.',NULL),(153,1,'2019-03-05 11:50:15','User','User logged in.',NULL),(154,1,'2019-03-05 11:51:48','User','User logged in.',NULL),(155,1,'2019-03-05 11:52:35','User','User logged in.',NULL),(156,1,'2019-03-05 11:57:09','User','User logged in.',NULL),(157,1,'2019-03-05 12:11:56','User','User logged in.',NULL),(158,1,'2019-03-05 12:13:52','User','User logged in.',NULL),(159,1,'2019-03-05 12:15:04','User','User logged in.',NULL),(160,1,'2019-03-06 08:56:58','User','User logged in.',NULL),(161,1,'2019-03-06 08:59:09','Pages Manager','Changed private from private to public for Page #101 and stripped re_auth.','::1'),(162,1,'2019-03-06 08:59:09','Pages Manager','Added 3 permission(s) to essenswahl_heute.php.','::1'),(163,1,'2019-03-06 09:10:26','Pages Manager','Changed private from private to public for Page #102 and stripped re_auth.','::1'),(164,1,'2019-03-06 09:10:26','Pages Manager','Added 3 permission(s) to essenswahl_nächste.php.','::1'),(165,1,'2019-03-06 09:29:28','Pages Manager','Changed private from private to public for Page #103 and stripped re_auth.','::1'),(166,1,'2019-03-06 09:29:28','Pages Manager','Added 3 permission(s) to essenswahl_diese.php.','::1'),(167,1,'2019-03-06 13:57:08','User','User logged in.',NULL),(168,1,'2019-03-06 13:59:04','User','User logged in.',NULL),(169,1,'2019-03-06 14:01:40','Pages Manager','Changed private from private to public for Page #104 and stripped re_auth.','::1'),(170,1,'2019-03-06 14:01:40','Pages Manager','Added 3 permission(s) to essensliste_diese.php.','::1'),(171,1,'2019-03-06 14:04:44','Pages Manager','Changed private from private to public for Page #105 and stripped re_auth.','::1'),(172,1,'2019-03-06 14:04:44','Pages Manager','Added 3 permission(s) to essensliste_nächste.php.','::1'),(173,1,'2019-03-06 15:02:02','User','User logged in.',NULL),(174,1,'2019-03-06 16:34:41','User','User logged in.',NULL),(175,1,'2019-03-06 18:17:50','User','User logged in.',NULL),(176,1,'2019-03-06 18:24:40','User','User logged in.',NULL),(177,1,'2019-03-06 18:33:05','User','User logged in.',NULL),(178,2,'2019-03-07 07:07:51','User','User logged in.',NULL),(179,1,'2019-03-07 07:08:36','User','User logged in.',NULL),(180,1,'2019-03-07 07:08:51','Pages Manager','Changed private from private to public for Page #106 and stripped re_auth.','::1'),(181,1,'2019-03-07 07:08:52','Pages Manager','Added 3 permission(s) to essensauswahl_diese.php.','::1'),(182,12,'2019-03-07 07:09:21','User','User logged in.',NULL),(183,14,'2019-03-07 07:15:29','User','Registration completed.','::1'),(184,14,'2019-03-07 07:15:44','User','User logged in.',NULL),(185,1,'2019-03-07 07:18:43','User','User logged in.',NULL),(186,1,'2019-03-07 07:19:38','Menu Manager','Added new item','::1'),(187,1,'2019-03-07 07:21:13','Menu Manager','Deleted menu 44','::1'),(188,2,'2019-03-19 11:57:34','User','User logged in.',NULL),(189,1,'2019-03-19 11:58:36','User','User logged in.',NULL),(190,1,'2019-03-19 12:02:38','Pages Manager','Added 1 permission(s) to usersc/templates/simplex/preview.php.','::1'),(191,12,'2019-03-19 12:15:05','User','User logged in.',NULL),(192,15,'2019-03-19 12:18:08','User','Registration completed.','::1'),(193,15,'2019-03-19 12:18:30','User','User logged in.',NULL),(194,12,'2019-03-21 07:01:09','User','User logged in.',NULL),(195,2,'2019-03-21 07:03:58','User','User logged in.',NULL),(196,1,'2019-03-21 07:06:57','User','User logged in.',NULL),(197,1,'2019-03-26 14:33:46','User','User logged in.',NULL),(198,12,'2019-03-26 14:36:54','User','User logged in.',NULL),(199,12,'2019-03-26 14:38:21','User','User logged in.',NULL),(200,12,'2019-03-26 14:47:37','User','User logged in.',NULL),(201,2,'2019-03-26 14:49:27','User','User logged in.',NULL),(202,1,'2019-03-26 14:56:08','User','User logged in.',NULL),(203,12,'2019-03-26 14:58:08','User','User logged in.',NULL),(204,12,'2019-03-28 16:26:16','User','User logged in.',NULL),(205,12,'2019-04-01 09:49:58','User','User logged in.',NULL),(206,12,'2019-04-01 10:16:10','User','User logged in.',NULL),(207,12,'2019-04-01 10:19:44','User','User logged in.',NULL),(208,12,'2019-04-01 10:20:53','User','User logged in.',NULL),(209,12,'2019-04-03 15:24:48','User','User logged in.',NULL),(210,12,'2019-04-03 15:37:51','User','User logged in.',NULL),(211,12,'2019-04-03 15:38:46','User','User logged in.',NULL);
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logs_exempt`
--

DROP TABLE IF EXISTS `logs_exempt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logs_exempt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `createdby` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `logs_exempt_type` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logs_exempt`
--

LOCK TABLES `logs_exempt` WRITE;
/*!40000 ALTER TABLE `logs_exempt` DISABLE KEYS */;
/*!40000 ALTER TABLE `logs_exempt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `menu_title` varchar(255) NOT NULL,
  `parent` int(10) NOT NULL,
  `dropdown` int(1) NOT NULL,
  `logged_in` int(1) NOT NULL,
  `display_order` int(10) NOT NULL,
  `label` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `icon_class` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'main',2,0,0,1,'Startseite','index.php','fa fa-fw fa-Startseite'),(3,'main',-1,0,1,11,'{{username}}','users/account.php','fa fa-fw fa-user'),(4,'main',-1,1,0,99,'Help','wie_funktionierts.php','fa fa-fw fa-life-ring'),(5,'main',-1,0,0,19,'Register','users/join.php','fa fa-fw fa-plus-square'),(6,'main',-1,0,0,18,'Log In','users/login.php','fa fa-fw fa-sign-in'),(7,'main',2,0,1,2,'Account','users/account.php','fa fa-fw fa-user'),(8,'main',2,0,1,3,'{{hr}}','',''),(9,'main',2,0,1,4,'Admin Dashboard','users/admin.php','fa fa-fw fa-cogs'),(10,'main',2,0,1,5,'User Management','users/admin.php?view=users','fa fa-fw fa-user'),(11,'main',2,0,1,6,'Permissions Manager','users/admin.php?view=permissions','fa fa-fw fa-lock'),(12,'main',2,0,1,7,'Page Management','users/admin.php?view=pages','fa fa-fw fa-wrench'),(13,'main',2,0,1,8,'Messages Manager','users/admin.php?view=messages','fa fa-fw fa-envelope'),(14,'main',2,0,1,9,'System Logs','users/admin.php?view=logs','fa fa-fw fa-search'),(15,'main',2,0,1,10,'{{hr}}','',''),(16,'main',2,0,1,11,'Logout','users/logout.php','fa fa-fw fa-sign-out'),(19,'main',4,0,0,20,'Forgot Password','users/forgot_password.php','fa fa-fw fa-wrench'),(20,'main',-1,0,1,12,'{{notifications}}','',''),(21,'main',-1,0,1,13,'{{messages}}','',''),(22,'main',4,0,0,99999,'Resend Activation Email','users/verify_resend.php','fa fa-exclamation-triangle'),(23,'main',-1,0,0,1,'Startseite','index.php','fa fa-fw-startseite'),(24,'main',25,0,1,21,'Essenstage','essenstage.php','fa fa-fw-essenstage'),(25,'main',-1,1,1,7,'Essen verwalten','#','fa fa-fw-kueche'),(26,'main',-1,0,0,3,'{{hr}}','',''),(27,'main',-1,1,1,14,'','','fa fa-fw fa-cogs'),(28,'main',27,0,1,4,'Dashboard','users/admin.php','fa fa-fw fa-cogs'),(29,'main',27,0,1,5,'User Management','users/admin.php?view=users','fa fa-fw fa-user'),(30,'main',27,0,1,6,'Permission Manager','users/admin.php?view=permissions','fa fa-fw fa-lock'),(31,'main',27,0,1,7,'Page Management','users/admin.php?view=pages','fa fa-fw fa-wrench'),(32,'main',27,0,1,3,'{{hr}}','',''),(33,'main',27,0,1,1,'Startseite','index.php','fa fa-fw fa-home'),(34,'main',27,0,1,2,'Account','users/account.php','fa fa-fw fa-user'),(40,'main',25,0,1,22,'neue Essen','essens_eingabe.php','fa fa-fw-neu'),(36,'main',27,0,1,8,'Message Managemet','users/admin.php?view=messages','fa fa-fw fa-envelope'),(37,'main',27,0,1,9,'System Logs','users/admin.php?view=logs','fa fa-fw fa-search'),(38,'main',27,0,1,10,'{{hr}}','',''),(39,'main',27,0,1,11,'Log out','users/logout.php','fa fa-fw fa-sign-out'),(41,'main',-1,0,1,6,'Essen ausw&auml;hlen','essensauswahl.php','fa fa-fw-auswahl'),(42,'main',-1,0,0,3,'Essensliste','essensliste_diese.php','fa fa-fw-essensliste'),(43,'main',-1,0,1,5,'Meine Essenswahl','essenswahl_heute.php','fa fa-fw fa-essenswahl');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message_threads`
--

DROP TABLE IF EXISTS `message_threads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_threads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_to` int(11) NOT NULL,
  `msg_from` int(11) NOT NULL,
  `msg_subject` varchar(255) NOT NULL,
  `last_update` datetime NOT NULL,
  `last_update_by` int(11) NOT NULL,
  `archive_from` int(1) NOT NULL DEFAULT 0,
  `archive_to` int(1) NOT NULL DEFAULT 0,
  `hidden_from` int(1) NOT NULL DEFAULT 0,
  `hidden_to` int(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message_threads`
--

LOCK TABLES `message_threads` WRITE;
/*!40000 ALTER TABLE `message_threads` DISABLE KEYS */;
INSERT INTO `message_threads` VALUES (1,2,1,'Testiing123','2017-08-06 00:13:47',1,0,1,0,0),(2,2,1,'Testing Message Badge','2017-09-09 15:10:09',1,0,1,0,0);
/*!40000 ALTER TABLE `message_threads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_from` int(11) NOT NULL,
  `msg_to` int(11) NOT NULL,
  `msg_body` text NOT NULL,
  `msg_read` int(1) NOT NULL,
  `msg_thread` int(11) NOT NULL,
  `deleted` int(1) NOT NULL,
  `sent_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (1,1,2,'&lt;p&gt;fgds&lt;/p&gt;',1,1,0,'2017-08-06 00:13:47'),(2,1,2,'&lt;p&gt;Did it work?&lt;/p&gt;',1,2,0,'2017-09-09 15:10:09');
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `message` mediumtext NOT NULL,
  `is_read` tinyint(4) NOT NULL,
  `is_archived` tinyint(1) DEFAULT 0,
  `date_created` datetime DEFAULT NULL,
  `date_read` datetime DEFAULT NULL,
  `last_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page` varchar(100) NOT NULL,
  `title` varchar(50) NOT NULL,
  `private` int(11) NOT NULL DEFAULT 0,
  `re_auth` int(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'index.php','Home',0,0),(2,'z_us_root.php','',0,0),(3,'users/account.php','Account Dashboard',1,0),(4,'users/admin.php','Admin Dashboard',1,0),(14,'users/forgot_password.php','Forgotten Password',0,0),(15,'users/forgot_password_reset.php','Reset Forgotten Password',0,0),(16,'users/index.php','Home',0,0),(17,'users/init.php','',0,0),(18,'users/join.php','Join',0,0),(19,'users/joinThankYou.php','Join',0,0),(20,'users/login.php','Login',0,0),(21,'users/logout.php','Logout',0,0),(24,'users/user_settings.php','User Settings',1,0),(25,'users/verify.php','Account Verification',0,0),(26,'users/verify_resend.php','Account Verification',0,0),(28,'usersc/empty.php','',0,0),(31,'users/oauth_success.php','',0,0),(33,'users/fb-callback.php','',0,0),(38,'users/google_helpers.php','',0,0),(41,'users/messages.php','Messages',1,0),(42,'users/message.php','Messages',1,0),(45,'users/maintenance.php','Maintenance',0,0),(49,'users/admin_verify.php','Password Verification',1,0),(68,'users/update.php','Update Manager',1,0),(74,'users/admin_notifications.php','Notifications Manager',1,0),(76,'users/enable2fa.php','Enable 2 Factor Auth',1,0),(77,'users/disable2fa.php','Disable 2 Factor Auth',1,0),(82,'users/manage2fa.php','Manage Two FA',1,0),(83,'users/manage_sessions.php','Session Manage',1,0),(86,'users/SSP.php','',1,0),(87,'users/features.ini.php','',1,0),(88,'users/loader.php','',1,0),(89,'users/twofa.php','',1,0),(90,'bearbeiten_essensliste.php','',1,0),(91,'db_connect.php','',1,0),(92,'essensauswahl.php','',0,0),(93,'essens_bearbeiten.php','',1,0),(94,'essens_eingabe.php','K&uuml;che',0,0),(96,'essenstage.php','Essenstage',0,0),(97,'wie_funktionierts.php','Help',0,0),(101,'essenswahl_heute.php','',0,0),(102,'essenswahl_nächste.php','',0,0),(103,'essenswahl_diese.php','',0,0),(104,'essensliste_diese.php','',0,0),(105,'essensliste_nächste.php','',0,0),(106,'essensauswahl_diese.php','',0,0);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_page_matches`
--

DROP TABLE IF EXISTS `permission_page_matches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_page_matches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_id` int(15) NOT NULL,
  `page_id` int(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_page_matches`
--

LOCK TABLES `permission_page_matches` WRITE;
/*!40000 ALTER TABLE `permission_page_matches` DISABLE KEYS */;
INSERT INTO `permission_page_matches` VALUES (2,2,27),(3,1,24),(4,1,22),(5,2,13),(6,2,12),(7,1,11),(8,2,10),(9,2,9),(10,2,8),(11,2,7),(12,2,6),(13,2,5),(14,2,4),(15,1,3),(16,2,37),(17,2,39),(19,2,40),(21,2,41),(23,2,42),(27,1,42),(28,1,27),(29,1,41),(30,1,40),(31,2,44),(32,2,47),(33,2,51),(34,2,50),(35,2,49),(36,2,53),(37,2,52),(38,2,68),(39,2,55),(40,2,56),(41,2,71),(42,2,58),(43,2,57),(44,2,53),(45,2,74),(46,2,75),(47,1,75),(48,1,76),(49,2,76),(50,1,77),(51,2,77),(52,2,78),(53,2,80),(54,1,81),(55,1,82),(56,1,83),(57,2,84),(58,1,92),(59,1,95),(60,2,95),(61,3,95),(62,1,98),(63,2,98),(64,3,98),(65,3,94),(66,3,93),(67,3,90),(68,1,101),(69,2,101),(70,3,101),(71,1,102),(72,2,102),(73,3,102),(74,1,103),(75,2,103),(76,3,103),(77,1,104),(78,2,104),(79,3,104),(80,1,105),(81,2,105),(82,3,105),(83,1,106),(84,2,106),(85,3,106),(86,2,107);
/*!40000 ALTER TABLE `permission_page_matches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'User'),(2,'Administrator'),(3,'K&uuml;che');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `bio` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profiles`
--

LOCK TABLES `profiles` WRITE;
/*!40000 ALTER TABLE `profiles` DISABLE KEYS */;
INSERT INTO `profiles` VALUES (1,1,'&lt;h1&gt;This is the Admin&#039;s bio.&lt;/h1&gt;'),(2,2,'This is your bio');
/*!40000 ALTER TABLE `profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `recaptcha` int(1) NOT NULL DEFAULT 0,
  `force_ssl` int(1) NOT NULL,
  `css_sample` int(1) NOT NULL,
  `us_css1` varchar(255) NOT NULL,
  `us_css2` varchar(255) NOT NULL,
  `us_css3` varchar(255) NOT NULL,
  `site_name` varchar(100) NOT NULL,
  `language` varchar(255) NOT NULL,
  `track_guest` int(1) NOT NULL,
  `site_offline` int(1) NOT NULL,
  `force_pr` int(1) NOT NULL,
  `glogin` int(1) NOT NULL DEFAULT 0,
  `fblogin` int(1) NOT NULL,
  `gid` varchar(255) NOT NULL,
  `gsecret` varchar(255) NOT NULL,
  `gredirect` varchar(255) NOT NULL,
  `ghome` varchar(255) NOT NULL,
  `fbid` varchar(255) NOT NULL,
  `fbsecret` varchar(255) NOT NULL,
  `fbcallback` varchar(255) NOT NULL,
  `graph_ver` varchar(255) NOT NULL,
  `finalredir` varchar(255) NOT NULL,
  `req_cap` int(1) NOT NULL,
  `req_num` int(1) NOT NULL,
  `min_pw` int(2) NOT NULL,
  `max_pw` int(3) NOT NULL,
  `min_un` int(2) NOT NULL,
  `max_un` int(3) NOT NULL,
  `messaging` int(1) NOT NULL,
  `snooping` int(1) NOT NULL,
  `echouser` int(11) NOT NULL,
  `wys` int(1) NOT NULL,
  `change_un` int(1) NOT NULL,
  `backup_dest` varchar(255) NOT NULL,
  `backup_source` varchar(255) NOT NULL,
  `backup_table` varchar(255) NOT NULL,
  `msg_notification` int(1) NOT NULL,
  `permission_restriction` int(1) NOT NULL,
  `auto_assign_un` int(1) NOT NULL,
  `page_permission_restriction` int(1) NOT NULL,
  `msg_blocked_users` int(1) NOT NULL,
  `msg_default_to` int(1) NOT NULL,
  `notifications` int(1) NOT NULL,
  `notif_daylimit` int(3) NOT NULL,
  `recap_public` varchar(100) NOT NULL,
  `recap_private` varchar(100) NOT NULL,
  `page_default_private` int(1) NOT NULL,
  `navigation_type` tinyint(1) NOT NULL,
  `copyright` varchar(255) NOT NULL,
  `custom_settings` int(1) NOT NULL,
  `system_announcement` varchar(255) NOT NULL,
  `twofa` int(1) DEFAULT 0,
  `force_notif` tinyint(1) DEFAULT NULL,
  `cron_ip` varchar(255) DEFAULT NULL,
  `registration` tinyint(1) DEFAULT NULL,
  `join_vericode_expiry` int(9) unsigned NOT NULL,
  `reset_vericode_expiry` int(9) unsigned NOT NULL,
  `admin_verify` tinyint(1) NOT NULL,
  `admin_verify_timeout` int(9) NOT NULL,
  `session_manager` tinyint(1) NOT NULL,
  `template` varchar(255) DEFAULT 'default',
  `saas` tinyint(1) DEFAULT NULL,
  `redirect_uri_after_login` text DEFAULT NULL,
  `show_tos` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,0,0,0,'../users/css/color_schemes/bootstrap.min.css','../users/css/sb-admin.css','../users/css/custom.css','Essen-Martinschule','en',1,0,0,0,0,'','','','','','','','','',0,0,6,30,4,30,1,1,0,1,0,'/','everything','',0,0,0,0,0,1,0,7,'6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI','6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe',1,1,'Jasper Jansen',1,'',0,0,'off',1,24,15,1,120,0,'simplex',NULL,NULL,0);
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `updates`
--

DROP TABLE IF EXISTS `updates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `updates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `migration` varchar(15) NOT NULL,
  `applied_on` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `update_skipped` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `updates`
--

LOCK TABLES `updates` WRITE;
/*!40000 ALTER TABLE `updates` DISABLE KEYS */;
INSERT INTO `updates` VALUES (15,'1XdrInkjV86F','2018-02-18 22:33:24',NULL),(16,'3GJYaKcqUtw7','2018-04-25 16:51:08',NULL),(17,'3GJYaKcqUtz8','2018-04-25 16:51:08',NULL),(18,'69qa8h6E1bzG','2018-04-25 16:51:08',NULL),(19,'2XQjsKYJAfn1','2018-04-25 16:51:08',NULL),(20,'549DLFeHMNw7','2018-04-25 16:51:08',NULL),(21,'4Dgt2XVjgz2x','2018-04-25 16:51:08',NULL),(22,'VLBp32gTWvEo','2018-04-25 16:51:08',NULL),(23,'Q3KlhjdtxE5X','2018-04-25 16:51:08',NULL),(24,'ug5D3pVrNvfS','2018-04-25 16:51:08',NULL),(25,'69FbVbv4Jtrz','2018-04-25 16:51:09',NULL),(26,'4A6BdJHyvP4a','2018-04-25 16:51:09',NULL),(27,'37wvsb5BzymK','2018-04-25 16:51:09',NULL),(28,'c7tZQf926zKq','2018-04-25 16:51:09',NULL),(29,'ockrg4eU33GP','2018-04-25 16:51:09',NULL),(30,'XX4zArPs4tor','2018-04-25 16:51:09',NULL),(31,'pv7r2EHbVvhD','2018-04-26 00:00:00',NULL),(32,'uNT7NpgcBDFD','2018-04-26 00:00:00',NULL),(33,'mS5VtQCZjyJs','2018-12-11 14:19:16',NULL),(34,'23rqAv5elJ3G','2018-12-11 14:19:51',NULL),(35,'2ZB9mg1l0JXe','2019-03-02 20:33:36',NULL),(36,'4vsCCmI9CuNZ','2019-03-02 20:33:36',NULL),(37,'B9t6He7qmFXa','2019-03-02 20:33:36',NULL);
/*!40000 ALTER TABLE `updates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `us_announcements`
--

DROP TABLE IF EXISTS `us_announcements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `us_announcements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dismissed` int(11) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `ignore` varchar(50) DEFAULT NULL,
  `class` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `us_announcements`
--

LOCK TABLES `us_announcements` WRITE;
/*!40000 ALTER TABLE `us_announcements` DISABLE KEYS */;
INSERT INTO `us_announcements` VALUES (1,3,'https://www.userspice.com/updates','New Version','December 11, 2018 - Thank you for trying UserSpice Beta!','4.5.0','warning');
/*!40000 ALTER TABLE `us_announcements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `us_fingerprint_assets`
--

DROP TABLE IF EXISTS `us_fingerprint_assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `us_fingerprint_assets` (
  `kFingerprintAssetID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fkFingerprintID` int(11) NOT NULL,
  `IP_Address` varchar(255) NOT NULL,
  `User_Browser` varchar(255) NOT NULL,
  `User_OS` varchar(255) NOT NULL,
  PRIMARY KEY (`kFingerprintAssetID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `us_fingerprint_assets`
--

LOCK TABLES `us_fingerprint_assets` WRITE;
/*!40000 ALTER TABLE `us_fingerprint_assets` DISABLE KEYS */;
/*!40000 ALTER TABLE `us_fingerprint_assets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `us_fingerprints`
--

DROP TABLE IF EXISTS `us_fingerprints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `us_fingerprints` (
  `kFingerprintID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fkUserID` int(11) NOT NULL,
  `Fingerprint` varchar(32) NOT NULL,
  `Fingerprint_Expiry` datetime NOT NULL,
  `Fingerprint_Added` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`kFingerprintID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `us_fingerprints`
--

LOCK TABLES `us_fingerprints` WRITE;
/*!40000 ALTER TABLE `us_fingerprints` DISABLE KEYS */;
/*!40000 ALTER TABLE `us_fingerprints` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `us_form_validation`
--

DROP TABLE IF EXISTS `us_form_validation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `us_form_validation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `params` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `us_form_validation`
--

LOCK TABLES `us_form_validation` WRITE;
/*!40000 ALTER TABLE `us_form_validation` DISABLE KEYS */;
INSERT INTO `us_form_validation` VALUES (1,'min','Minimum # of Characters','number'),(2,'max','Maximum # of Characters','number'),(3,'is_numeric','Must be a number','true'),(4,'valid_email','Must be a valid email address','true'),(5,'<','Must be a number less than','number'),(6,'>','Must be a number greater than','number'),(7,'<=','Must be a number less than or equal to','number'),(8,'>=','Must be a number greater than or equal to','number'),(9,'!=','Must not be equal to','text'),(10,'==','Must be equal to','text'),(11,'is_integer','Must be an integer','true'),(12,'is_timezone','Must be a valid timezone name','true'),(13,'is_datetime','Must be a valid DateTime','true');
/*!40000 ALTER TABLE `us_form_validation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `us_form_views`
--

DROP TABLE IF EXISTS `us_form_views`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `us_form_views` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_name` varchar(255) NOT NULL,
  `view_name` varchar(255) NOT NULL,
  `fields` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `us_form_views`
--

LOCK TABLES `us_form_views` WRITE;
/*!40000 ALTER TABLE `us_form_views` DISABLE KEYS */;
/*!40000 ALTER TABLE `us_form_views` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `us_forms`
--

DROP TABLE IF EXISTS `us_forms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `us_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `us_forms`
--

LOCK TABLES `us_forms` WRITE;
/*!40000 ALTER TABLE `us_forms` DISABLE KEYS */;
/*!40000 ALTER TABLE `us_forms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `us_ip_blacklist`
--

DROP TABLE IF EXISTS `us_ip_blacklist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `us_ip_blacklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(50) NOT NULL,
  `last_user` int(11) NOT NULL DEFAULT 0,
  `reason` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `us_ip_blacklist`
--

LOCK TABLES `us_ip_blacklist` WRITE;
/*!40000 ALTER TABLE `us_ip_blacklist` DISABLE KEYS */;
INSERT INTO `us_ip_blacklist` VALUES (3,'192.168.0.21',1,0),(4,'192.168.0.22',1,0),(10,'192.168.0.222',0,0);
/*!40000 ALTER TABLE `us_ip_blacklist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `us_ip_list`
--

DROP TABLE IF EXISTS `us_ip_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `us_ip_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `us_ip_list`
--

LOCK TABLES `us_ip_list` WRITE;
/*!40000 ALTER TABLE `us_ip_list` DISABLE KEYS */;
INSERT INTO `us_ip_list` VALUES (1,'::1',12,'2019-03-26 14:58:08');
/*!40000 ALTER TABLE `us_ip_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `us_ip_whitelist`
--

DROP TABLE IF EXISTS `us_ip_whitelist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `us_ip_whitelist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `us_ip_whitelist`
--

LOCK TABLES `us_ip_whitelist` WRITE;
/*!40000 ALTER TABLE `us_ip_whitelist` DISABLE KEYS */;
INSERT INTO `us_ip_whitelist` VALUES (2,'192.168.0.21'),(3,'192.168.0.23');
/*!40000 ALTER TABLE `us_ip_whitelist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `us_plugins`
--

DROP TABLE IF EXISTS `us_plugins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `us_plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plugin` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `updates` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `us_plugins`
--

LOCK TABLES `us_plugins` WRITE;
/*!40000 ALTER TABLE `us_plugins` DISABLE KEYS */;
/*!40000 ALTER TABLE `us_plugins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `us_saas_levels`
--

DROP TABLE IF EXISTS `us_saas_levels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `us_saas_levels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` varchar(255) NOT NULL,
  `users` int(11) NOT NULL,
  `details` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `us_saas_levels`
--

LOCK TABLES `us_saas_levels` WRITE;
/*!40000 ALTER TABLE `us_saas_levels` DISABLE KEYS */;
/*!40000 ALTER TABLE `us_saas_levels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `us_saas_orgs`
--

DROP TABLE IF EXISTS `us_saas_orgs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `us_saas_orgs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `org` varchar(255) NOT NULL,
  `owner` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `active` int(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `us_saas_orgs`
--

LOCK TABLES `us_saas_orgs` WRITE;
/*!40000 ALTER TABLE `us_saas_orgs` DISABLE KEYS */;
/*!40000 ALTER TABLE `us_saas_orgs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `us_user_sessions`
--

DROP TABLE IF EXISTS `us_user_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `us_user_sessions` (
  `kUserSessionID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fkUserID` int(11) unsigned NOT NULL,
  `UserFingerprint` varchar(255) NOT NULL,
  `UserSessionIP` varchar(255) NOT NULL,
  `UserSessionOS` varchar(255) NOT NULL,
  `UserSessionBrowser` varchar(255) NOT NULL,
  `UserSessionStarted` datetime NOT NULL,
  `UserSessionLastUsed` datetime DEFAULT NULL,
  `UserSessionLastPage` varchar(255) NOT NULL,
  `UserSessionEnded` tinyint(1) NOT NULL DEFAULT 0,
  `UserSessionEnded_Time` datetime DEFAULT NULL,
  PRIMARY KEY (`kUserSessionID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `us_user_sessions`
--

LOCK TABLES `us_user_sessions` WRITE;
/*!40000 ALTER TABLE `us_user_sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `us_user_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_permission_matches`
--

DROP TABLE IF EXISTS `user_permission_matches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_permission_matches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_permission_matches`
--

LOCK TABLES `user_permission_matches` WRITE;
/*!40000 ALTER TABLE `user_permission_matches` DISABLE KEYS */;
INSERT INTO `user_permission_matches` VALUES (100,1,1),(101,1,2),(102,2,1),(111,11,1),(112,12,1),(113,12,3),(114,13,1),(115,14,1),(116,15,1);
/*!40000 ALTER TABLE `user_permission_matches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(155) NOT NULL,
  `email_new` varchar(155) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `pin` varchar(255) DEFAULT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `permissions` int(11) NOT NULL,
  `logins` int(100) NOT NULL,
  `account_owner` tinyint(4) NOT NULL DEFAULT 0,
  `account_id` int(11) NOT NULL DEFAULT 0,
  `company` varchar(255) NOT NULL,
  `join_date` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `email_verified` tinyint(4) NOT NULL DEFAULT 0,
  `vericode` varchar(15) NOT NULL,
  `vericode_expiry` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `active` int(1) NOT NULL,
  `oauth_provider` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `oauth_uid` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `gpluslink` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `picture` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `fb_uid` varchar(255) NOT NULL,
  `un_changed` int(1) NOT NULL,
  `msg_exempt` int(1) NOT NULL DEFAULT 0,
  `last_confirm` datetime DEFAULT NULL,
  `protected` int(1) NOT NULL DEFAULT 0,
  `dev_user` int(1) NOT NULL DEFAULT 0,
  `msg_notification` int(1) NOT NULL DEFAULT 1,
  `force_pr` int(1) NOT NULL DEFAULT 0,
  `twoKey` varchar(16) DEFAULT NULL,
  `twoEnabled` int(1) DEFAULT 0,
  `twoDate` datetime DEFAULT NULL,
  `cloak_allowed` tinyint(1) NOT NULL DEFAULT 0,
  `org` int(11) DEFAULT NULL,
  `account_mgr` int(11) DEFAULT 0,
  `oauth_tos_accepted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `EMAIL` (`email`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'userspicephp@gmail.com',NULL,'admin','$2y$12$1v06jm2KMOXuuo3qP7erTuTIJFOnzhpds1Moa8BadnUUeX0RV3ex.',NULL,'The','Admin',1,50,1,0,'UserSpice','2016-01-01 00:00:00','2019-03-26 15:56:08',1,'nlPsJDtyeqFWsS','2019-03-26 14:56:08',0,'','','','','','','0000-00-00 00:00:00','1899-11-30 00:00:00','',0,1,'2017-10-08 15:24:37',1,0,1,0,NULL,0,NULL,0,NULL,0,1),(2,'noreply@userspice.com',NULL,'user','$2y$12$HZa0/d7evKvuHO8I3U8Ff.pOjJqsGTZqlX8qURratzP./EvWetbkK',NULL,'Sample','User',1,14,1,0,'none','2016-01-02 00:00:00','2019-03-26 15:49:27',1,'2ENJN4xD8nnjOgk','2019-03-26 14:49:27',1,'','','','','','','0000-00-00 00:00:00','0000-00-00 00:00:00','',0,0,NULL,0,0,1,0,NULL,0,NULL,0,NULL,0,1),(11,'berta@cool.de',NULL,'Jonathan','$2y$12$Lty/MMf68z2my.yw4f/BWOrF4Gld7xbbjUfkIZSoOAa7ORvyGXkd.',NULL,'Jansen','Jonathan',1,1,1,0,'','2019-03-03 19:15:27','2019-03-03 19:15:51',1,'KJ1eem71ZWsodOY','2019-03-03 18:15:51',1,'','','','','','','0000-00-00 00:00:00','0000-00-00 00:00:00','',0,0,NULL,0,0,1,0,NULL,0,NULL,0,NULL,0,1),(12,'kueche@martinschule.de',NULL,'K&uuml;che','$2y$12$oFGoNV1yNH4nvg9MFO6tluXeGaSa8tF4Emiq/a1jdjv11Cpweqx9e',NULL,'K&uuml;che','Lecker',1,20,1,0,'','2019-03-03 21:28:04','2019-04-03 17:38:46',1,'h4vTwtfXoJveN7h','2019-04-03 15:38:46',1,'','','','','','','0000-00-00 00:00:00','0000-00-00 00:00:00','',0,0,NULL,0,0,1,0,NULL,0,NULL,0,NULL,0,1),(13,'hoppeangela@web.de',NULL,'yogi','$2y$12$aiYS8AVXflVHioe1Le3lo.CmUaRr2xwZAWqkCW/dOuHlvlTBmdOhK',NULL,'Angela','Hoppe',1,1,1,0,'','2019-03-03 22:02:35','2019-03-03 22:03:24',1,'aHaGqeLiiqyK63P','2019-03-03 21:03:24',1,'','','','','','','0000-00-00 00:00:00','0000-00-00 00:00:00','',0,0,NULL,0,0,1,0,NULL,0,NULL,0,NULL,0,1),(14,'Jasper@mail.de',NULL,'Horsti','$2y$12$/u9/uVC7Euyxbaq.nUdpg.1Xn3URSQfuaoAr3iB.7uY4a50QQ8T5u',NULL,'Horst','M&uuml;ller',1,1,1,0,'','2019-03-07 08:15:29','2019-03-07 08:15:44',1,'njyVmzsalHmAmC','2019-03-07 07:15:44',1,'','','','','','','0000-00-00 00:00:00','0000-00-00 00:00:00','',0,0,NULL,0,0,1,0,NULL,0,NULL,0,NULL,0,1),(15,'Spring@toll.de',NULL,'Spring','$2y$12$ulXrpI1VKF/ykCajQoSsCevO/OAujzUn8Ndg0ST1neZTeiI6cWAaq',NULL,'Ludwig','Spring',1,1,1,0,'','2019-03-19 13:18:07','2019-03-19 13:18:30',1,'m1Q7cldclSs6rgK','2019-03-19 12:18:30',1,'','','','','','','0000-00-00 00:00:00','0000-00-00 00:00:00','',0,0,NULL,0,0,1,0,NULL,0,NULL,0,NULL,0,1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_online`
--

DROP TABLE IF EXISTS `users_online`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_online` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ip` varchar(15) NOT NULL,
  `timestamp` varchar(15) NOT NULL,
  `user_id` int(10) NOT NULL,
  `session` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_online`
--

LOCK TABLES `users_online` WRITE;
/*!40000 ALTER TABLE `users_online` DISABLE KEYS */;
INSERT INTO `users_online` VALUES (1,'::1','1553612279',1,''),(3,'::1','1553611994',2,''),(4,'::1','1551637108',11,''),(5,'::1','1554306094',12,''),(6,'::1','1551647067',13,''),(7,'::1','1551943094',14,''),(8,'::1','1552997946',15,'');
/*!40000 ALTER TABLE `users_online` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_session`
--

DROP TABLE IF EXISTS `users_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `uagent` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_session`
--

LOCK TABLES `users_session` WRITE;
/*!40000 ALTER TABLE `users_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_session` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-03 18:25:50
