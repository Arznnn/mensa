<?php
/*
UserSpice 4
An Open Source PHP User Management System
by the UserSpice Team at http://UserSpice.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
?>
<?php
require_once 'users/init.php';
require_once $abs_us_root.$us_url_root.'users/includes/header.php';
require_once $abs_us_root.$us_url_root.'users/includes/navigation.php';
?>

<?php if (!securePage($_SERVER['PHP_SELF'])){die();} ?>

<div id="page-wrapper">
	<div class="container-fluid">
		<!-- Page Heading -->
		<div class="row">
			<div class="col-sm-12">
				<h1 class="page-header">
					Neue Essen
				</h1>
				<!-- Content goes here -->
				<a href="essens_bearbeiten.php">Bearbeiten</a>
				<?php
				require 'db_connect.php';
				if (isset($_POST['aktion']) and $_POST['aktion']=='speichern') {
				    $name = "";
				    if (isset($_POST['name'])) {
				        $name = trim($_POST['name']);
				    }
				    $farbe = "";
				    if (isset($_POST['farbe'])) {
				        $farbe = trim($_POST['farbe']);
				    }
					    // speichern
					 $einfuegen = $mysqli->prepare(
				                 "INSERT INTO essensliste (name, farbe)
				                 VALUES (?, ?)"
				                 );
				        $einfuegen->bind_param('ss', $name, $farbe);
				        if ($einfuegen->execute()) {
				//            header('Location: index.php?aktion=feedbackgespeichert');
				//            die();
					}
				    }
				?>

				<form action="" method="post">
				    <label>Name:
				        <input type="text" name="name" id="name">
					</label>
				    <label>Farbe:
				  <select name="farbe">
				     <option>Bitte auswählen ...</option>
				     <option>Grün</option>
				     <option>Rot</option>
				    </select>
				    </label>
				    <input type="hidden" name="aktion" value="speichern">
				    <input type="submit" value="hinzufügen">
				</form>


				<?php
				$daten = array();

				if ($erg = $mysqli->query("SELECT * FROM essensliste")) {
				    if ($erg->num_rows) {
				        while($datensatz = $erg->fetch_object()) {
				            $daten[] = $datensatz;
				        }
				        $erg->free();
				    }
				}

				if (!count($daten)) {
				    echo "<p>Es liegen keine Daten vor :(</p>";
				} else {
				?>
				    <table>
				        <thead>
				            <tr>
				                <th>Name</th>
				                <th>Farbe</th>
				            </tr>
				        </thead>
				        <tbody>
				            <?php
				            foreach ($daten as $inhalt) {
				            ?>
						 <tr>
				                    <td><?php echo bereinigen($inhalt->name);	?>	</td>
				                    <td><?php echo bereinigen($inhalt->farbe);	?>	</td>
				                </tr>
				            <?php
				            }
				            ?>
				        </tbody>
				    </table>
				<?php
				}
				function bereinigen($inhalt='') {
				    $inhalt = trim($inhalt);
				    $inhalt = htmlentities($inhalt, ENT_QUOTES, "UTF-8");
				    return($inhalt);
				}
				$mysqli->close();

				?>
				<!-- Content Ends Here -->
			</div> <!-- /.col -->
		</div> <!-- /.row -->
	</div> <!-- /.container -->
</div> <!-- /.wrapper -->


<?php require_once $abs_us_root.$us_url_root.'users/includes/page_footer.php'; // the final html footer copyright row + the external js calls ?>

<!-- Place any per-page javascript here -->

<?php require_once $abs_us_root.$us_url_root.'users/includes/html_footer.php'; // currently just the closing /body and /html ?>
